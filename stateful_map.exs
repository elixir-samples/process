defmodule StatefulMap do
  def start do
    spawn(fn -> loop%{} end)
  end

  # Parent process
  def put(pid, key, value) do
    send(pid, {:put, key, value})
  end

  def get(pid, key) do
    send(pid, {:get, key, self()})

    receive do
      {:response, value} -> value
    end
  end

  # Child process
  def loop(current) do
    new_state = receive do
      message -> process(current, message)
    end

    loop(new_state)
  end

  defp process(current, {:put, key, value}) do
    Map.put(current, key, value)
  end

  defp process(current, {:get, key, caller}) do
    send(caller, {:response, Map.get(current, key)})
    current
  end
end
